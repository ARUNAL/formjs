import React from 'react';
import './Counter.css'

class Counter extends React.Component {
    constructor() {
        super()
        this.state = {
            counter: 0

        }
        this.increment = this.increment.bind(this)
    }
    render() {
        return (
            <div className="Counter">

                <button onClick={this.increment}>+1</button>

                <span className="Counter">{this.state.counter}</span>

            </div>
        );
    }



    increment() {

        // this.state.counter++
        this.setState({
            counter: this.state.counter + 1
        })
    }
}
export default Counter;
