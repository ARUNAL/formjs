import React, { useState } from 'react';
// import logo from './logo.svg';
import Cards from './Cards';
import Form from './Form'

const App = () => {
    const [Values, setValues] = useState([
        {
            name: 'lal',
            email: 'lal@outlook.com',
            mobile: 989898989,
            message: 'hai'
        },
        {
            name: 'vinay',
            email: 'vinay@outlook.com',
            mobile: 989898989,
            message: 'hi there!'
        },
        {
            name: 'sandhya',
            email: 'sandhya09@gmail.com',
            mobile: 898988899,
            message: 'hello everyone'

        }
    ])
    const [showvalue, setShowValue] = useState(true)
    const deleteCardHandler = (cardIndex) => {
        const Values_copy = [...Values]
        console.log(Values)
        console.log(Values_copy)
        Values_copy.splice(cardIndex, 1)

        setValues(Values_copy)

    }
    const ButtonMarkUp = showvalue &&
        Values.map((card, index) => (<Cards name={Values.name}
            message={Values.messgae}
            key={Values.id}
            onDelete={() => deleteCardHandler(index)}


        ></Cards>))
    const toggleShowCard = () => setShowValue(!showvalue)
    return (

        <div className="App">
            <Form />
            <button onClick={toggleShowCard}>Visite Collection</button>
            {ButtonMarkUp}

        </div>
    );
}

export default App;
