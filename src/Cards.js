import React from 'react';
import './App.css';

const Card = props => {

    return (
        <div className="App">
            <div className="card">
                <div className="container">
                    <h4><b>{props.name}</b></h4>

                    <p>{props.email}</p>

                    <p><button onClick={props.onDelete}>Delete</button></p>
                    <div>{props.children}</div>
                </div>
            </div>

        </div>
    )

}
export default Card