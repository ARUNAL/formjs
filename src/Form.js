import React from 'react';
import { Formik } from 'formik';

const Form = (props) => (
    <div>
        <h1>Anywhere in your app!</h1>
        <Formik
            initialValues={{ email: '', name: '', mobile: '', message: '' }}
            validate={values => {
                const errors = {};
                if (!values.email) {
                    errors.email = 'Required';
                } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                    errors.email = 'Invalid email address';
                }
                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                console.log(values)
                console.log('email ' + values.email)
                console.log('name ' + values.name)
                console.log('mobile ' + values.mobile)


            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
            }) => (
                    <form onSubmit={handleSubmit}>
                        <br /><input
                            type="name"
                            name="name"
                            onChange={handleChange}
                            value={values.name}
                        />
                        {errors.name && touched.name && errors.name}
                        <br /><input
                            type="email"
                            name="email"
                            onChange={handleChange}
                            value={values.email}
                        />
                        {errors.email && touched.email && errors.email}

                        <br /><input type="mobile"
                            name="mobile"
                            onChange={handleChange}
                            value={values.mobile}
                        />
                        {errors.mobile && touched.mobile && errors.mobile}
                        <br /><textarea type="message"
                            name="message"
                            onChange={handleChange}
                            value={values.message}
                        />
                        {errors.message && touched.message && errors.message}
                        <button type="submit" disabled={isSubmitting}>
                            Submit
           </button>
                    </form>
                )}
        </Formik>
    </div>
);

export default Form;